//
//  FBAuthStrategy.swift
//  URemont
//
//  Created by User on 05/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin

open class FacebookAuthProvider: AuthProvider {

    public func authorize(completion: @escaping (_ result: SocialProfile?, _ error: Error?) -> ()) {
            let fbLoginManager: LoginManager = LoginManager()

            func makeGraphRequest(accessToken: AccessToken) {
                GraphRequest(graphPath: "/me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"], accessToken: accessToken, httpMethod: .GET, apiVersion: .defaultVersion).start({ (response, result) in

                    switch result {
                    case .success(let response):
                        if let json = response.dictionaryValue {
                            let id = json["id"] as? String
                            let name = json["name"] as? String
                            let email = json["email"] as? String

                            let userProfile = SocialProfile(
                                UID: id ?? "-",
                                name: name ?? "-",
                                email: email ?? "-",
                                token: accessToken.authenticationToken
                            )

                            completion(userProfile, nil)
                        } else {
                            let error = NSError(domain: "", code: 999, userInfo: [NSLocalizedDescriptionKey: "JSON serialization error"])
                            completion(nil, error)
                        }
                    case .failed(let error):
                        completion(nil, error)
                    }
                })
            }

            if AccessToken.current == nil {
                fbLoginManager.logIn([ReadPermission.publicProfile, ReadPermission.email], viewController: UIApplication.topViewController()) { loginResult in
                switch loginResult {
                    case .failed(let error):
                        completion(nil, error)
                    case .cancelled:
                        let error = NSError(domain: "", code: 999, userInfo: [NSLocalizedDescriptionKey: "Login cancelled."])
                        completion(nil, error)
                    case .success( _, _, let accessToken):
                        makeGraphRequest(accessToken: accessToken)
                    }
                }
            } else {
                makeGraphRequest(accessToken: AccessToken.current!)
            }

        }

    public func signOut() {
        LoginManager().logOut()
    }

}


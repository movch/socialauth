//
//  YandexAuthProvider.swift
//  SocialAuth
//
//  Created by ARomanov on 11/04/2017.
//  Copyright © 2017 Michail Ovchinnikov. All rights reserved.
//

import UIKit
import PromiseKit

open class YandexAuthProvider: NSObject, AuthProvider{
    
    fileprivate let deferred = Promise<SocialProfile>.pending()
    fileprivate var yandexCallbackUrl: String?
    fileprivate var yandexAppId: String?
    
    override init() {
        super.init()
        deferred.promise.always { print(self) }
    }
    
    public func authorize(completion: @escaping (SocialProfile?, Error?) -> ()) {
        self.yandexAuthorize()
            .then { response -> Void in
                completion(response, nil)
            }
            .catch { (error: Error) in
                NSLog(error.localizedDescription)
                completion(nil, error)
        }
    }
    
    public func signOut() {
        // Log out
    }
    
    fileprivate func yandexAuthorize() -> Promise<SocialProfile> {
        if let callbackUrl = Bundle.main.infoDictionary!["YandexCallbackUrl"] as? String, let appId = Bundle.main.infoDictionary!["YandexAppId"] as? String{
            yandexCallbackUrl = callbackUrl
            yandexAppId = appId
            if let topController = UIApplication.topViewController(){
                let viewController = UIViewController()
                viewController.view.backgroundColor = .white
                var size = viewController.view.frame.size
                size.height -= 20
                let frame = CGRect(origin: CGPoint(x:0, y:20), size: size)
                let webView = UIWebView(frame: frame)
                let urlRequest = URLRequest(url: URL(string: "https://oauth.yandex.ru/authorize?response_type=token&client_id=\(yandexAppId ?? "")")!)
                webView.loadRequest(urlRequest)
                webView.delegate = self
                viewController.view.addSubview(webView)
                topController.present(viewController, animated: true, completion: nil)
            }
        }
        else{
            NSLog("[YandexAuthProvider] В Info.plist не указаны данные по приложению!")
        }
        return deferred.promise
    }
    
    
    
    fileprivate func parseJson(data: Data?, token: String){
        if data == nil{
            let error = NSError(domain: "", code: 999, userInfo: [NSLocalizedDescriptionKey: "Ошибка получения данных пользователя Яндекс"])
            deferred.reject(error)
            return
        }
        let jsonData = data!
        // Convert server json response to NSDictionary
        do {
            if let json = try JSONSerialization.jsonObject(with: jsonData, options: []) as? NSDictionary {
                // Get value by key
                let email = json["default_email"] as? String
                let name = json["real_name"] as? String
                let id = json["id"] as? String
                let userProfile = SocialProfile(UID: id ?? "-", name: name ?? "-", email:  email ?? "-", token: token)
                self.deferred.fulfill(userProfile)
            }
            else{
                let error = NSError(domain: "", code: 999, userInfo: [NSLocalizedDescriptionKey: "Ошибка сериализации json"])
                deferred.reject(error)
            }
        } catch let error as NSError {
            self.deferred.reject(error)
        }
        
    }
    
    fileprivate func getYandexProfile(token: String){
        if let url = URL(string: "https://login.yandex.ru/info?format=json&oauth_token=\(token)"){
            let task = URLSession.shared.dataTask(with: url) { data, response, error in
                if let webView = UIApplication.topViewController(){
                    webView.dismiss(animated: true, completion: nil)
                }
                guard error == nil else {
                    self.deferred.reject(error!)
                    return
                }
                self.parseJson(data: data, token: token)
            }
            task.resume()
        }
        else{
            let error = NSError(domain: "", code: 999, userInfo: [NSLocalizedDescriptionKey: "Ошибка в URL. Получение данных пользователя Яндекс"])
            deferred.reject(error)
        }
    }
    
    fileprivate func getAccessToken(urlString: String){
        let matches = urlString.matches(for: "access_token=(.*?)(&|$)")
        if matches.count > 0{
            let index = matches[0].index(matches[0].startIndex, offsetBy: 13)
            var token = matches[0].substring(from: index)
            token.characters.removeLast()
            self.getYandexProfile(token: token)
        }
        else{
            let error = NSError(domain: "", code: 999, userInfo: [NSLocalizedDescriptionKey: "Ошибка получения Яндекс токена"])
            deferred.reject(error)
        }
    }
}


extension YandexAuthProvider: UIWebViewDelegate{
    
    public func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        if let url = request.url{
            if yandexCallbackUrl != nil && url.absoluteString.hasPrefix("\(yandexCallbackUrl!)#access_token="){
                getAccessToken(urlString: url.absoluteString)
            }
        }
        return true
    }
    
}


//
//  VKAuthProvider.swift
//  SocialAuth
//
//  Created by ARomanov on 11/04/2017.
//  Copyright © 2017 Michail Ovchinnikov. All rights reserved.
//

import PromiseKit
import VK_ios_sdk

open class VKAuthProvider: NSObject, AuthProvider {
    
    private let deferred = Promise<SocialProfile>.pending()
    
    override init() {
        super.init()
        deferred.promise.always { print(self) }
    }
    
    public func authorize(completion: @escaping (SocialProfile?, Error?) -> ()) {
        self.vkAuthorize()
            .then { response -> Void in
                completion(response, nil)
            }
            .catch { (error: Error) in
                NSLog(error.localizedDescription)
                completion(nil, error)
        }
    }
    
    public func signOut() {
        //Log out func()
    }
    
    private func vkAuthorize() -> Promise<SocialProfile> {
        if let vkAppId = Bundle.main.infoDictionary!["VkAppID"] as? String{
            let VKSdkInstance = VKSdk.initialize(withAppId: vkAppId)
            
            VKSdkInstance!.register(self)
            VKSdkInstance!.uiDelegate = self
            let scope = ["wall"]
            VKSdk.authorize(scope)
        }
        else{
            NSLog("[VKAuthProvider] Info.plist doesn't have VkAppId key/value")
        }
        return deferred.promise
    }
    
    // MARK: functions for handling and parsing result
    
    fileprivate func handleResponse(result: VKAuthorizationResult){
        if (result.state == VKAuthorizationState.authorized) {
            parseUser(vkUser: result.user, token: result.token.accessToken)
        }
        else if result.state == .pending{
            NSLog("pending auth info, please, wait....")
        }
        else if result.state == .error{
            let error = NSError(domain: "", code: 999, userInfo: [NSLocalizedDescriptionKey: "Ошибка входа через ВК"])
            deferred.reject(error)
        }
    }
    
    fileprivate func parseUser(vkUser: VKUser?, token: String){
        if let user = vkUser{
            let id = user.id.stringValue
            let name = "\(user.last_name ?? "") \(user.first_name ?? "")"
            let site = user.site
            let token = token
            let userProfile = SocialProfile(UID: id, name: name, email: site ?? "-", token: token)
            deferred.fulfill(userProfile)
        }
        else{
            let error = NSError(domain: "", code: 999, userInfo: [NSLocalizedDescriptionKey: "Отсутствуют данные пользователя ВК"])
            deferred.reject(error)
        }
        
    }
}


extension VKAuthProvider: VKSdkDelegate, VKSdkUIDelegate {
   
    @objc public func vkSdkUserAuthorizationFailed() {
        NSLog("[VKAuthProvider] vkSdkUserAuthorizationFailed")
    }
    
    @objc public func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        NSLog("[VKAuthProvider] vkSdkNeedCaptchaEnter")
    }
    
    @objc public func vkSdkAccessTokenUpdated(_ newToken: VKAccessToken!, oldToken: VKAccessToken!) {
        NSLog("[VKAuthProvider] vkSdkAccessTokenUpdated")
    }
    
    @objc public func vkSdkDidDismiss(_ controller: UIViewController!) {
        NSLog("[VKAuthProvider] vkSdkDidDismiss")
    }
    
    @objc public func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        NSLog("[VKAuthProvider] vkSdkAccessAuthorizationFinished")
        self.handleResponse(result: result)
    }
    
    @objc public func vkSdkAuthorizationStateUpdated(with result: VKAuthorizationResult!) {
        NSLog("[VKAuthProvider] vkSdkAuthorizationStateUpdated")
        self.handleResponse(result: result)
    }
    
    @objc public func vkSdkWillDismiss(_ controller: UIViewController!) {
        NSLog("[VKAuthProvider] vkSdkWillDismiss")
    }
    
    @objc public func vkSdkShouldPresent(_ controller: UIViewController!) {
        NSLog("[VKAuthProvider] vkSdkShouldPresent")
        
        if let topController = UIApplication.topViewController() {
            topController.present(controller, animated: true, completion: nil)
        }
    }
    
    @objc public func vkSdkTokenHasExpired(_ expiredToken: VKAccessToken!) {
        NSLog("[VKAuthProvider] vkSdkTokenHasExpired")
    }
}


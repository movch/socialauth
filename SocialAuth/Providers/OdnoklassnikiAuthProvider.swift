//
//  OdnoklassnikiAuthProvider.swift
//  SocialAuth
//
//  Created by User on 22/05/2017.
//  Copyright © 2017 Michail Ovchinnikov. All rights reserved.
//

import Foundation

import PromiseKit

class OdnoklassnikiAuthProvider: NSObject, AuthProvider {
    
    fileprivate let deferred = Promise<SocialProfile>.pending()
    fileprivate var odnoklassnikiAppId: String?
    fileprivate var odnoklassnikiCallbackUrl: String?
    
    override init() {
        super.init()
        deferred.promise.always { print(self) }
    }
    
    public func authorize(completion: @escaping (SocialProfile?, Error?) -> ()) {
        self.okAuthorize()
            .then { response -> Void in
                completion(response, nil)
            }
            .catch { (error: Error) in
                NSLog(error.localizedDescription)
                completion(nil, error)
        }
    }
    
    public func signOut() {
        print("odnoklassniki sign out")
    }
    
    fileprivate func okAuthorize() -> Promise<SocialProfile> {
        if let callbackUrl = Bundle.main.infoDictionary!["OdnoklassnikiCallbackUrl"] as? String, let appId = Bundle.main.infoDictionary!["OdnoklassnikiAppId"] as? String{
            odnoklassnikiAppId = appId
            odnoklassnikiCallbackUrl = callbackUrl
            
            if let topController = UIApplication.topViewController(){
                let viewController = UIViewController()
                viewController.view.backgroundColor = .white
                var size = viewController.view.frame.size
                size.height -= 20
                let frame = CGRect(origin: CGPoint(x:0, y:20), size: size)
                let webView = UIWebView(frame: frame)
                
                let strUrl = getAuthStringUrl()
                if let url = URL(string: strUrl){
                    let urlRequest = URLRequest(url: url)
                    webView.loadRequest(urlRequest)
                    webView.delegate = self
                    viewController.view.addSubview(webView)
                    topController.present(viewController, animated: true, completion: nil)
                }
                else{
                    NSLog("[OdnoklassnikiAuthProvider] invalid auth url request")
                }
                
            }
        }
        else{
            NSLog("[OdnoklassnikiAuthProvider] В Info.plist не указаны данные по приложению!")
        }
        return deferred.promise
    }
    
    fileprivate func getAccessToken(urlString: String){
        //session_secret_key=(.*?)&expires_in=(.*?)($)
            let matches = urlString.matches(for: "access_token=(.*?)(&|$)")
            if matches.count > 0{
                let index = matches[0].index(matches[0].startIndex, offsetBy: 13)
                var token = matches[0].substring(from: index)
                token.characters.removeLast()
                
                //WARNING, there is no data, except token in SocialProfile
                let profile = SocialProfile(UID: "", name: "", email: "", token: token)
                deferred.fulfill(profile)
                if let topController = UIApplication.topViewController(){
                    topController.dismiss(animated: true, completion: nil)
                }

            }
            else{
                let error = NSError(domain: "", code: 999, userInfo: [NSLocalizedDescriptionKey: "Ошибка получения Яндекс токена"])
                deferred.reject(error)
            }
    }
    
    //https://apiok.ru/ext/oauth/client
    fileprivate func getAuthStringUrl() -> String{
        //Идентификатор приложения {application id}
        let clientId = odnoklassnikiAppId!
        //Запрашиваемые права приложения, разделённые символом. См. права приложения
        let scope = "VALUABLE_ACCESS&scope=LONG_ACCESS_TOKEN"
        //Тип ответа от сервера
        let responseType = "token"
        //redirect_uri URI, на который будет передан access_token. URI должен посимвольно совпадать с одним из URI, зарегистрированных в настройках приложения.
        let redirectUri = odnoklassnikiCallbackUrl!
        //Внешний вид окна авторизации: 
        //* m – окно для мобильной авторизации;
        //* a – упрощённое окно для мобильной авторизации без шапки.
        let layout = "m"
        return "https://connect.ok.ru/oauth/authorize?client_id=\(clientId)&scope=\(scope)&response_type=\(responseType)&redirect_uri=\(redirectUri)&layout=\(layout)"

    }

}

extension OdnoklassnikiAuthProvider: UIWebViewDelegate{
    
    public func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        if let url = request.url{
            print("\n\n"+url.absoluteString)
            if odnoklassnikiCallbackUrl != nil && url.absoluteString.hasPrefix("\(odnoklassnikiCallbackUrl!)#access_token="){
                getAccessToken(urlString: url.absoluteString)
            }
        }
        return true
    }
    
}

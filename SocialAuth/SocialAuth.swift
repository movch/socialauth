//
//  PromiseAuth.swift
//  URemont
//
//  Created by User on 01/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import Foundation

open class SocialAuth {
    
    private var provider: AuthProvider
    
    public init(provider: SocialAuthNetwork) {
        self.provider = provider.authProvider()
    }
    
    public func authorize(completion: @escaping (_ result: SocialProfile?, _ error: Error?) -> ()) {
        return provider.authorize(completion: completion)
    }
    
    public func signOut() {
        provider.signOut()
    }
    
}

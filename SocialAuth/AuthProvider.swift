//
//  AuthStrategy.swift
//  URemont
//
//  Created by User on 01/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import Foundation

public protocol AuthProvider {

    func authorize(completion: @escaping (_ result: SocialProfile?, _ error: Error?) -> ())
    func signOut()

}

//
//  AuthBaseProfile.swift
//  URemont
//
//  Created by User on 08/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import Foundation

public struct SocialProfile {

    public var UID: String
    public var name: String
    public var email: String
    public var token: String

}

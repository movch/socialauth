//
//  SocialNetwork.swift
//  URemont
//
//  Created by User on 08/12/2016.
//  Copyright © 2016 AppCraft. All rights reserved.
//

import Foundation

public enum SocialAuthNetwork: Int {
    
    case fb = 1
    case vk = 2
    case yandex = 3
    case odnoklassniki = 4
    
    func authProvider() -> AuthProvider {
        switch self {
        case .fb:
            return FacebookAuthProvider()
        case .vk:
            return VKAuthProvider()
        case .yandex:
            return YandexAuthProvider()
        case .odnoklassniki:
            return OdnoklassnikiAuthProvider()
        }
    }
    
    func getNetworkName() -> String {
        switch self {
        case .fb:
            return "Facebook"
        case .vk:
            return "VK"
        case .yandex:
            return "Yandex"
        case .odnoklassniki:
            return "Одноклассники"
        }
    }
    
}

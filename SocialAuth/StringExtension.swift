//
//  StringExtension.swift
//  SocialAuth
//
//  Created by User on 13/04/2017.
//  Copyright © 2017 Michail Ovchinnikov. All rights reserved.
//

import Foundation
extension String{
    
    /**
     Get all matches in string by regex
     
     - parameter regex: Regex for check
     */
    public func matches(for regex: String) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let nsString = self as NSString
            let results = regex.matches(in: self, range: NSRange(location: 0, length: nsString.length))
            return results.map { nsString.substring(with: $0.range)}
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
}

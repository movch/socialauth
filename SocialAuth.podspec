Pod::Spec.new do |s|
  s.platform = :ios
  s.ios.deployment_target = '9.0'
  s.name = "SocialAuth"
  s.summary = "Simple Swift wrapper for popular social networks."
  s.requires_arc = true
  s.version = "0.1.0"
  s.license = { :type => "MIT", :file => "LICENSE" }
  s.author = { "Michail Ovchinnikov" => "michail@ovchinnikov.cc" }
  s.homepage = "https://bitbucket.org/movch/socialauth" 
  s.source = { :git => "https://movch@bitbucket.org/movch/socialauth.git", :tag => "#{s.version}"}
  s.source_files = "SocialAuth/*.{swift}"
 
  s.subspec 'Facebook' do |fb|
    fb.source_files = "SocialAuth/Providers/FacebookAuthProvider.swift"
    fb.dependency 'FacebookCore', '~> 0.2'
    fb.dependency 'FacebookLogin', '~> 0.2'
  end

  s.subspec 'VK' do |vk|
    vk.source_files = "SocialAuth/Providers/VKAuthProvider.swift"
    vk.dependency 'PromiseKit', '~> 4.0'
    vk.dependency 'VK-ios-sdk', '~> 1.4'
  end

  s.subspec 'Yandex' do |yandex|
    yandex.source_files = "SocialAuth/Providers/YandexAuthProvider.swift"
    yandex.dependency 'PromiseKit', '~> 4.0'
  end

   # OK API integration
  s.subspec 'Odnoklassniki' do |odnoklassniki|
    odnoklassniki.source_files = "SocialAuth/Providers/OdnoklassnikiAuthProvider.swift"
    odnoklassniki.dependency 'PromiseKit', '~> 4.0'
  end

end

# SocialAuth

**SocialAuth** is a Swift wrapper for simple authentication in popular social networks for iOS platform.

## Installation

Note: you will need to set up all IDs, URL schemes, etc. in Info.plist **manually**, it is also assumed that you already have all required account(s) in social network(s) you need. Don't add any social network related pods to the Podfile, they will be handled automatically. Below you can find quick start instructions for all supported social networks.

### Facebook

Add to your Podfile:

    pod 'SocialAuth', git: 'https://movch@bitbucket.org/movch/socialauth.git'
    pod 'SocialAuth/Facebook', git: 'https://movch@bitbucket.org/movch/socialauth.git'

[Register your application at Facebook](https://developers.facebook.com/apps/)

Add to Info.plist (Right-click > Open As > Source code):

    <key>FacebookAppID</key>
    <string>{ PUT APP ID HERE }</string>
    <key>FacebookDisplayName</key>
    <string>{ PUT APP NAME HERE }</string>

    <key>LSApplicationQueriesSchemes</key>
    <array>
        <string>fbapi</string>
        <string>fb-messenger-api</string>
        <string>fbauth2</string>
        <string>fbshareextension</string>
    </array>

Go to Project > Info tab. Create URL Scheme:

    - fb{app_id}
    - Role: Editor

Edit `AppDelegate.swift` :

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
       if let fromApplication = options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String {
            let options:[UIApplicationOpenURLOptionsKey: Any] = [
                UIApplicationOpenURLOptionsKey.sourceApplication: fromApplication,
                UIApplicationOpenURLOptionsKey.annotation: fromApplication
            ]
            SDKApplicationDelegate.shared.application(app, open: url, options: options)
        }

        return true
    }

Edit `AppDelegate.swift` :

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // ...
        // Facebook
        SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        // ...
        return true
    }

### VK

Add to your Podfile:

    pod 'SocialAuth', git: 'https://movch@bitbucket.org/movch/socialauth.git'
    pod 'SocialAuth/VK', git: 'https://movch@bitbucket.org/movch/socialauth.git'

[Register your application at VK](https://vk.com/dev)

Add to Info.plist (Right-click > Open As > Source code):

    <key>VkAppID</key>
    <string>{ PUT APP ID HERE }</string>

    <key>LSApplicationQueriesSchemes</key>
    <array>
        <string>vk</string>
        <string>vk-share</string>
        <string>vkauthorize</string>
    </array>

Go to Project > Info tab. Create URL Scheme:

    - vk{app_id}
    - Role: Editor

Edit `AppDelegate.swift` :

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        // ...
        // VK
        if let fromApplication = options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String{
            VKSdk.processOpen(url, fromApplication: fromApplication)
        }
        // ...
        return true
    }

### Yandex

Add to your Podfile:

    pod 'SocialAuth', git: 'https://movch@bitbucket.org/movch/socialauth.git'
    pod 'SocialAuth/Yandex', git: 'https://movch@bitbucket.org/movch/socialauth.git'

[Register your application at Yandex](https://oauth.yandex.ru/client/new)

Add to Info.plist (Right-click > Open As > Source code):

    <key>YandexCallbackUrl</key>
    <string>{ PUT CALLBACK URL HERE }</string>

    <key>YandexAppId</key>
    <string>{ PUT YANDEX APP ID HERE }</string>
    
### Odnoklassniki

Add to your Podfile:

    pod 'SocialAuth', git: 'https://movch@bitbucket.org/movch/socialauth.git'
    pod 'SocialAuth/Odnoklassniki', git: 'https://movch@bitbucket.org/movch/socialauth.git'

[Register your application at ok.ru](https://ok.ru)
Open tab with Games -> My uploads

Add to Info.plist (Right-click > Open As > Source code):

    <key>OdnoklassnikiAppId</key>
	<string>{ PUT ODNOKLASSNIKI APP ID HERE }</string>
    
	<key>OdnoklassnikiCallbackUrl</key>
	<string>{ PUT ODNOKLASSNIKI MOBILE CALLBACK HERE (ok{APP_ID}://authorize) }</string>
    
Go to Project > Info tab. Create two URL Schemes:

    - ok{app_id}
    - Role: Editor
    
    - okauth
    - Role: Editor
    

## Usage

Import SocialAuth module:

    import SocialAuth

Initialize `SocialAuth` object with required provider and call `authorize()` method (example for Facebook auth):

    SocialAuth(provider: .fb).authorize() { result, error in
        if let profile = result {
            print(profile.token)
        } else {
            print(error?.localizedDescription ?? "Unknown error")
        }
    }

